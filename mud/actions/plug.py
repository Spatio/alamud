
from .action import Action3
from mud.events import PlugInEvent

class PlugInAction(Action3):
    EVENT = PlugInEvent
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "plug"
