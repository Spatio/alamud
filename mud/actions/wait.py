from .action import Action2
from mud.events import WaitEvent

class WaitAction(Action2):
    EVENT = WaitEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "wait"
