from .event import Event3

class PlugInEvent(Event3):
    NAME = "plug"

    def plug_failure(self):
        self.fail()
        self.inform("plug.failed")

    def perform(self):
        listeid = ["souris-000","casque-000","clavier-000"]

        if not self.object2.is_container():
            self.add_prop("object2-not-container")
            return self.plug_failure()

        if self.object not in self.actor:
            self.add_prop("object-not-in-inventory")
            return self.plug_failure()

        if not self.object2.has_prop("pluginable"):
            self.object.add_prop("object2-not-pluginable")
            return self.plug_failure()

        if not self.object.has_prop("plugable"):
            self.object.add_prop("object-not-plugable")
            return self.plug_failure()

        if not self.get_datum("plug.data-driven"):
            self.object.move_to(self.object2)

        itemsRassembles=0
        for i in self.object2.contents():
            if i.id in listeid:
                itemsRassembles+=1
        if itemsRassembles == len(listeid):
            self.object2.add_prop("fini")


        self.inform("plug")
