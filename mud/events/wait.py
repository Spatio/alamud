from .event import Event2

class WaitEvent(Event2):
    NAME = "wait"

    def perform(self):
        self.inform("wait")
